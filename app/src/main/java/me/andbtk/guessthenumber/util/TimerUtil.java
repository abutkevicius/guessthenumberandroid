package me.andbtk.guessthenumber.util;

import java.util.Date;
import java.util.Locale;

public class TimerUtil {
    private static final int MS_IN_SEC = 1000;
    private static final int SEC_IN_MIN = 60;

    public static String timeElapsed(Date start) {
        Date current = new Date();
        long diffInSeconds = (current.getTime() - start.getTime()) / MS_IN_SEC;

        return formatDuration(diffInSeconds);
    }

    public static String formatDuration(long duration) {
        long seconds = duration % SEC_IN_MIN;
        long minutes = duration / SEC_IN_MIN;

        return String.format(Locale.ENGLISH, "%02d:%02d", minutes, seconds);
    }
}
