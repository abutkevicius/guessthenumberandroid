package me.andbtk.guessthenumber.game;

import android.util.ArrayMap;

import lombok.Value;

@Value
public class Game {
    private State state;

    public boolean checkNumber(String number) {
        if (state.getNumber().equals(number)) {
            return true;
        }

        state.addHint(new Hint(number, findCorrectNumbers(number), findInPlace(number)));

        return false;
    }

    private int findCorrectNumbers(String input) {
        int correct = 0;
        ArrayMap<Character, Integer> solution = new ArrayMap<>();
        for (char digit :
                state.getNumber().toCharArray()) {
            if (solution.containsKey(digit)) {
                solution.put(digit, solution.get(digit) + 1);
            } else {
                solution.put(digit, 1);
            }
        }

        for (char candidate :
                input.toCharArray()) {
            if (solution.containsKey(candidate)) {
                if (solution.get(candidate) > 1) {
                    solution.put(candidate, solution.get(candidate) - 1);
                } else {
                    solution.remove(candidate);
                }
                correct++;
            }
        }

        return correct;
    }

    private int findInPlace(String input) {
        int inPlace = 0;
        char[] solution = state.getNumber().toCharArray();
        char[] inputChars = input.toCharArray();

        for (int i = 0; i < solution.length; i++) {
            if (solution[i] == inputChars[i]) {
                inPlace++;
            }
        }

        return inPlace;
    }
}
