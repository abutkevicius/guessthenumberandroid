package me.andbtk.guessthenumber.game;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;

import lombok.Value;

@Value
public class State {

    private String number;
    private Date startTime;
    private LinkedList<Hint> hints;

    public void addHint(Hint hint) {
        hints.addFirst(hint);
    }
}
