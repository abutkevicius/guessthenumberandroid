package me.andbtk.guessthenumber.game;

import java.util.Date;
import java.util.LinkedList;
import java.util.Random;

public class GameFactory {

    public static final int DEFAULT_DIGITS = 3;
    private Random random;

    public GameFactory(Random random) {
        this.random = random;
    }

    public Game create(int digits) {
        State state = new State(generateNumber(random, digits), new Date(), new LinkedList<Hint>());

        return new Game(state);
    }

    public Game create() {
        return create(DEFAULT_DIGITS);
    }

    private String generateNumber(Random random, int digits) {
        StringBuilder number = new StringBuilder();
        for (int i = 0; i < digits; i++) {
            number.append(Integer.toString(random.nextInt(10)));
        }

        return number.toString();
    }
}
