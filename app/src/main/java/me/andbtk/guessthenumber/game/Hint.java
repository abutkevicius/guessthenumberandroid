package me.andbtk.guessthenumber.game;

import lombok.Value;

@Value
public class Hint {

    private String guess;
    private int correct;
    private int inPlace;
}
