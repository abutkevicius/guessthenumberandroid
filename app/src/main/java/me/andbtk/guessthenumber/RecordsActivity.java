package me.andbtk.guessthenumber;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.List;
import java.util.Locale;

import me.andbtk.guessthenumber.entity.Result;
import me.andbtk.guessthenumber.util.TimerUtil;

public class RecordsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_records);

        List<Result> records = AppDatabaseFactory.build(this).resultDao().findRecords();

        if (records.size() > 0) {
            Result record = records.get(0);
            TextView date = findViewById(R.id.records_date);
            TextView attempts = findViewById(R.id.records_attempts);
            TextView duration = findViewById(R.id.records_duration);
            date.setText(DateFormat.getInstance().format(record.getStartTime()));
            attempts.setText(String.format(Locale.ENGLISH, "%d", record.getAttempts()));
            duration.setText(TimerUtil.formatDuration(record.getDuration()));
        }
        if (records.size() > 1) {
            Result record = records.get(1);
            TextView date = findViewById(R.id.records_date2);
            TextView attempts = findViewById(R.id.records_attempts2);
            TextView duration = findViewById(R.id.records_duration2);
            date.setText(DateFormat.getInstance().format(record.getStartTime()));
            attempts.setText(String.format(Locale.ENGLISH, "%d", record.getAttempts()));
            duration.setText(TimerUtil.formatDuration(record.getDuration()));
        }
    }
}
