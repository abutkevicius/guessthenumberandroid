package me.andbtk.guessthenumber;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.List;
import java.util.Locale;

import me.andbtk.guessthenumber.entity.Result;
import me.andbtk.guessthenumber.util.TimerUtil;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {

    private List<Result> results;

    public HistoryAdapter(List<Result> results) {
        this.results = results;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.history_list_row, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Result result = results.get(position);
        DateFormat df = DateFormat.getInstance();

        holder.date.setText("Date: " + df.format(result.getStartTime()));
        holder.attempts.setText("Attempts: " + String.format(Locale.ENGLISH, "%d", result.getAttempts()));
        holder.duration.setText("Time took: " + TimerUtil.formatDuration(result.getDuration()));
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public final TextView date;
        public final TextView attempts;
        public final TextView duration;

        public ViewHolder(View v) {
            super(v);

            date = v.findViewById(R.id.history_date);
            attempts = v.findViewById(R.id.history_attempts);
            duration = v.findViewById(R.id.history_duration);
        }
    }
}
