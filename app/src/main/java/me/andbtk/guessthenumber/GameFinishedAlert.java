package me.andbtk.guessthenumber;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class GameFinishedAlert {
    public static AlertDialog.Builder build(Context context, DialogInterface.OnClickListener positive, DialogInterface.OnClickListener negative) {
        return (new AlertDialog.Builder(context))
                .setMessage(R.string.won_message)
                .setTitle(R.string.won_title)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, positive)
                .setNegativeButton(R.string.play_again, negative);
    }
}
