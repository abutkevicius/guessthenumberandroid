package me.andbtk.guessthenumber;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view) {
        Intent intent;

        switch (view.getId()) {
            case R.id.playForTime:
                intent = new Intent(this, PlayActivity.class);
                startActivity(intent);
                break;
            case R.id.practise:
                intent = new Intent(this, PractiseActivity.class);
                startActivity(intent);
                break;
            case R.id.records:
                intent = new Intent(this, RecordsActivity.class);
                startActivity(intent);
                break;
            case R.id.history:
                intent = new Intent(this, HistoryActivity.class);
                startActivity(intent);
                break;
            case R.id.helpView:
                intent = new Intent(this, HelpActivity.class);
                startActivity(intent);
                break;
            default:
                Log.d(TAG, "Button not supported in onClick");
                break;
        }
    }
}
