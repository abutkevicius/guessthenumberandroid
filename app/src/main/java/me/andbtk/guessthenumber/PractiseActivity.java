package me.andbtk.guessthenumber;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import java.util.Random;

import me.andbtk.guessthenumber.game.Game;
import me.andbtk.guessthenumber.game.GameFactory;

public class PractiseActivity extends AppCompatActivity {

    private static final String TAG = "PractiseActivity";
    private Game game;
    private HintsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practise);

        GameFactory factory = new GameFactory(new Random());
        game = factory.create();

        RecyclerView recyclerView = findViewById(R.id.hints);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new HintsAdapter(game.getState().getHints());
        recyclerView.setAdapter(adapter);

        Log.i(TAG, String.format("Answer: %s", game.getState().getNumber()));
    }

    public void onClick(View view) {
        EditText guess = findViewById(R.id.guess);
        String input = guess.getText().toString().trim();
        int digitsCount = getResources().getInteger(R.integer.game_digits_count);
        if (input.length() < digitsCount) {
            guess.setError(getResources().getString(R.string.number_validation_error, digitsCount));
            return;
        }

        checkNumber(input);
    }

    public void onRestartClick(View view) {
        restartGame();
    }

    private void checkNumber(String number) {
        if (!game.checkNumber(number)) {
            adapter.notifyDataSetChanged();
            return;
        }

        GameFinishedAlert.build(this, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onBackPressed();
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                restartGame();
            }
        }).show();
    }

    private void restartGame() {
        onBackPressed();
        startActivity(new Intent(this, PractiseActivity.class));
    }
}
