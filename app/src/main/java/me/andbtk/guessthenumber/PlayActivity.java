package me.andbtk.guessthenumber;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Date;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import me.andbtk.guessthenumber.entity.Result;
import me.andbtk.guessthenumber.game.Game;
import me.andbtk.guessthenumber.game.GameFactory;
import me.andbtk.guessthenumber.game.State;
import me.andbtk.guessthenumber.util.TimerUtil;

public class PlayActivity extends AppCompatActivity {

    private static final String TAG = "PlayActivity";
    private Game game;
    private HintsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        GameFactory factory = new GameFactory(new Random());
        game = factory.create();

        final TextView timerView = findViewById(R.id.timer);
        final Date startTime = game.getState().getStartTime();
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        timerView.setText(TimerUtil.timeElapsed(startTime));
                    }
                });
            }
        }, 0, 1000);

        RecyclerView recyclerView = findViewById(R.id.hintsPlay);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new HintsAdapter(game.getState().getHints());
        recyclerView.setAdapter(adapter);

        Log.i(TAG, String.format("Answer: %s", game.getState().getNumber()));
    }

    public void onCheckNumberClick(View view) {
        EditText guess = findViewById(R.id.guessPlay);
        String input = guess.getText().toString().trim();
        int digitsCount = getResources().getInteger(R.integer.game_digits_count);
        if (input.length() < digitsCount) {
            guess.setError(getResources().getString(R.string.number_validation_error, digitsCount));
            return;
        }

        checkNumber(input);
    }

    public void onRestartClick(View view) {
        restartGame();
    }

    private void checkNumber(String number) {
        if (!game.checkNumber(number)) {
            adapter.notifyDataSetChanged();
            return;
        }

        State state = game.getState();
        AppDatabase db = AppDatabaseFactory.build(this);
        db.resultDao().insert(new Result(state.getHints().size(), state.getStartTime(), new Date()));


        GameFinishedAlert.build(this, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onBackPressed();
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                restartGame();
            }
        }).show();
    }

    private void restartGame() {
        onBackPressed();
        startActivity(new Intent(this, PlayActivity.class));
    }
}
