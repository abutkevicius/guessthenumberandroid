package me.andbtk.guessthenumber;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import me.andbtk.guessthenumber.dao.ResultDao;
import me.andbtk.guessthenumber.entity.Result;
import me.andbtk.guessthenumber.util.Converters;

@Database(entities = {Result.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract ResultDao resultDao();
}
