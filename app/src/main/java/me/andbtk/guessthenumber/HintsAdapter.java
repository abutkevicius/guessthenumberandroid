package me.andbtk.guessthenumber;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import me.andbtk.guessthenumber.game.Hint;

public class HintsAdapter extends RecyclerView.Adapter<HintsAdapter.ViewHolder> {

    private List<Hint> hints;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView hint;

        public ViewHolder(View v) {
            super(v);
            hint = v.findViewById(R.id.hintItem);
        }

    }

    public HintsAdapter(List<Hint> hints) {
        this.hints = hints;
    }

    @NonNull
    @Override
    public HintsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.hint_list_row, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Hint hint = hints.get(position);
        String text = String.format(Locale.ENGLISH,
                "<b>%d.</b> Your guess: <b>%s</b>; Digits guessed: <b>%d</b>; In its place: <b>%d</b>",
                getItemCount() - position,
                hint.getGuess(),
                hint.getCorrect(),
                hint.getInPlace()
        );

        holder.hint.setText(Html.fromHtml(text, 0));
    }

    @Override
    public int getItemCount() {
        return hints.size();
    }
}
