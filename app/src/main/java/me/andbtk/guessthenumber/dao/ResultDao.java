package me.andbtk.guessthenumber.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import me.andbtk.guessthenumber.entity.Result;

@Dao
public interface ResultDao {
    @Insert
    void insert(Result result);

    @Query("SELECT * FROM result ORDER BY start_time DESC")
    List<Result> getAll();

    @Query("SELECT * FROM result ORDER BY attempts ASC, duration ASC LIMIT 2")
    List<Result> findRecords();
}
