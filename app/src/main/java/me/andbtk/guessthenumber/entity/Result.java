package me.andbtk.guessthenumber.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import java.util.Date;

import lombok.Data;

@Entity(indices = {@Index("start_time"), @Index(value = {"attempts", "duration"})})
@Data
public class Result {
    @PrimaryKey(autoGenerate = true)
    private int id;

    private int attempts;

    @ColumnInfo(name = "start_time")
    private Date startTime;

    @ColumnInfo(name = "end_time")
    private Date endTime;

    private int duration;

    public Result(int attempts, Date startTime, Date endTime) {
        this.attempts = attempts;
        this.startTime = startTime;
        this.endTime = endTime;
        this.duration = (int) ((endTime.getTime() - startTime.getTime()) / 1000);
    }
}
