package me.andbtk.guessthenumber;

import android.arch.persistence.room.Room;
import android.content.Context;

public class AppDatabaseFactory {

    private static AppDatabase instance;

    public static AppDatabase build(Context context) {
        if (null == instance) {
            instance = Room
                    .databaseBuilder(context, AppDatabase.class, "guess-the-number")
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }

        return instance;
    }
}
